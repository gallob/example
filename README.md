This Project was originally taken from the java-smt/doc/java-smt-maven-example project. That's why the name 
and the execution instructions are exactly the same from the original project.

This project takes a single input String representing a crossword with mathematical equations.
It then extracts the formulas from the String, hands them to an SMT-Solver (default: Z3)
and prints the solution in the console, with all _ replaced by numbers.

If the constellation is unsatisfyable with numbers only ranging from 0 - 9 or because of incorrect input,
The program will print "This constellation is unsat".

Usage:

Create an input.txt file in the src folder (exactly there) and paste your String there.
-------------
IMPORTANT: Don't use leading whitespaces or finish the lines with artificial \n
All lines in the crossword must have the same length and only the classic arithmetic
operations are allowed (+, -, *, /).

If no crossword is detected an example will be used.
An example for a crossword in a correct format is provided here in the [input.txt](src/input.txt) in the src folder.
---------------

Once pasted continue with the original execution instructions from the project.
The Maven workflow in this project supports the following steps:

- Compiling: `mvn compile` downloads all dependencies and compiles the project.
- Testing: `mvn test` executes the Sudoku example test.
- Packaging: `mvn package` creates a jar file for the example application.
  Dependencies like other jar files and binaries for SMT solvers are stored in the directory `dependencies`.
- Running: `java -jar ./target/java-smt-maven-example-VERSION.jar` shows a table in the terminal.

Please note that the Maven repository currently only contains release versions
and no snapshots, thus the newest features and bugfixes might be missing.
If a Maven step is not working or more steps are required,
please ask or report an issue [in the project](https://github.com/sosy-lab/java-smt).

--------
BONUS QUESTIONS
--------
All solvers same model? 
No, some solvers, dont return any solutions at all some are even crashing before the program can return unsat. 

Why are some solvers not usable? What changes? Some solvers are not useable because it seems, that not all of them support the same operations. For example my problems were with the division. I had to ensure that the divisor is not zero before the actual constraint is being checked.

