package org.sosy_lab.java_smt_example;

public class Formula{
    private final Variable x;
    private final Variable y;
    private final Variable solution;
    private final char operator; // + - * /


    public Formula(MathCrossword mathCrossword, int index, int x, int y, int z, char operator, Directions direction){
        if(!mathCrossword.getVariables().containsKey(index)){
            this.x = new Variable(x, index);
            mathCrossword.getVariables().put(index, this.x);
        }else{
            this.x = mathCrossword.getVariables().get(index);
        }
        if(direction==Directions.SIDEWARDS){
            if(!mathCrossword.getVariables().containsKey(index+2)){
                this.y = new Variable(y, index+2);
                mathCrossword.getVariables().put(index+2, this.y);
            }else{
                this.y = mathCrossword.getVariables().get(index+2);
            }
        }
        else {
            if(!mathCrossword.getVariables().containsKey(index+2*mathCrossword.getLineLengthForNextRow())){
                this.y = new Variable(y, index+2*mathCrossword.getLineLengthForNextRow());
                mathCrossword.getVariables().put(index+2*mathCrossword.getLineLengthForNextRow(), this.y);
            }else{
                this.y = mathCrossword.getVariables().get(index+2*mathCrossword.getLineLengthForNextRow());
            }
        }
        if(direction==Directions.SIDEWARDS){
            if(!mathCrossword.getVariables().containsKey(index+4)){
                this.solution = new Variable(z, index+4);
                mathCrossword.getVariables().put(index+4, this.solution);
            }else{
                this.solution = mathCrossword.getVariables().get(index+4);
            }
        }
        else {
            if(!mathCrossword.getVariables().containsKey(index+4*mathCrossword.getLineLengthForNextRow())){
                this.solution = new Variable(z, index+4*mathCrossword.getLineLengthForNextRow());
                mathCrossword.getVariables().put(index+4*mathCrossword.getLineLengthForNextRow(), solution);
            }else{
                this.solution = mathCrossword.getVariables().get(index+4*mathCrossword.getLineLengthForNextRow());
            }

        }
        this.operator = operator;

    }

    public Variable getX() {
        return x;
    }


    public Variable getY() {
        return y;
    }



    public Variable getSolution() {
        return solution;
    }


    public char getOperator() {
        return operator;
    }




}