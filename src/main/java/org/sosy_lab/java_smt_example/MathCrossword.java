
package org.sosy_lab.java_smt_example;

import org.sosy_lab.common.ShutdownManager;
import org.sosy_lab.common.configuration.Configuration;
import org.sosy_lab.common.configuration.InvalidConfigurationException;
import org.sosy_lab.common.log.BasicLogManager;
import org.sosy_lab.common.log.LogManager;
import org.sosy_lab.java_smt.SolverContextFactory;
import org.sosy_lab.java_smt.api.*;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.*;


public class MathCrossword {
    private final String crossword; //crossword in single-String-format
    private final int lineLengthForNextRow; //to know in which position is a character below another in a Formula
    private final ArrayList<Formula> equations = new ArrayList<>(); //all detected equations
    private final HashMap<Integer, Variable> variables = new HashMap<>(); //all Variable-Objects
    private final ArrayList<BooleanFormula> formulaConstraints = new ArrayList<>();
    private final HashMap<Integer, NumeralFormula.IntegerFormula> formulaVariables = new HashMap<>();

    public MathCrossword(String input) {
        this.crossword = input;
        lineLengthForNextRow = findOutLineLength(input) + 1;
        for (int stringIndex = 0; stringIndex < input.length(); stringIndex++) {
            checkAndCreateFormula(input, stringIndex);
        }
    }

    public static void main(String[] args) throws InvalidConfigurationException {

        MathCrossword mathCrossword = new MathCrossword(MathCrossword.readFile());
        System.out.println("The detected crossword is: \n" + mathCrossword.getCrossword());
        Configuration config = Configuration.fromCmdLineArguments(args);
        LogManager logger = BasicLogManager.create(config);
        ShutdownManager shutdown = ShutdownManager.create();
        SolverContext context = SolverContextFactory.createSolverContext(
                config, logger, shutdown.getNotifier(), SolverContextFactory.Solvers.Z3);

        FormulaManager fmgr = context.getFormulaManager();
        BooleanFormulaManager bmgr = fmgr.getBooleanFormulaManager();
        IntegerFormulaManager imgr = fmgr.getIntegerFormulaManager();

        mathCrossword.createConstraints(mathCrossword, bmgr, imgr);

        try (ProverEnvironment prover = context.newProverEnvironment(SolverContext.ProverOptions.GENERATE_MODELS)) {
            prover.push(bmgr.and(mathCrossword.getFormulaConstraints()));
            boolean isUnsat = prover.isUnsat();
            if (isUnsat) {
                System.out.println("This constellation is unsat.");
            }
            Model model;
            if (!isUnsat) {
                model = prover.getModel();
                for (Model.ValueAssignment vari : model) {
                    try {
                        //TODO: schönere Lösung absprechen - insbesondere, wie man aus dem model werte herausbekommt.
                        mathCrossword.variables.get(Integer.valueOf(vari.getName())).
                                setValue(Integer.parseInt(vari.getValue().toString()));
                    } catch (Exception e) {
                        //ignore
                    }
                }
                System.out.println("--------------\n" + "SOLUTION\n" + "--------------\n");
                for (int i = 0; i < mathCrossword.getCrossword().length(); i++) {
                    if (mathCrossword.variables.containsKey(i)) {
                        System.out.print(mathCrossword.variables.get(i).getValue());
                    } else {
                        System.out.print(mathCrossword.getCrossword().charAt(i));
                    }
                }
                System.out.println();
            }
        } catch (InterruptedException | SolverException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * The key method. This method reads the formulas from the equations ArrayList in the MathCrossword class and
     * parses them into constraints and adds them to the formulaConstraints ArrayList. This ArrayList will then be
     * compatible to be given to the Prover-Object as List.
     *
     * @param mathCrossword mathCrossword object containing all Formulas and Variables as Lists
     * @param bmgr          Boolean Formula Manager used by JavaSMT (for logical and/or)
     * @param imgr          Integer Formula Manager used by JavaSMT (for equals, lessThan, greaterThan)
     */
    public void createConstraints(MathCrossword mathCrossword, BooleanFormulaManager bmgr, IntegerFormulaManager imgr) {
        for (Formula x : mathCrossword.getEquations()) {
            NumeralFormula.IntegerFormula xFormula = imgr.makeVariable(Integer.toString(x.getX().getIndexInString()));
            NumeralFormula.IntegerFormula yFormula = imgr.makeVariable(Integer.toString(x.getY().getIndexInString()));
            NumeralFormula.IntegerFormula zFormula = imgr.makeVariable(Integer.toString(x.getSolution().getIndexInString()));

            //If variable is not unknown ( " _ ") then we assign it it's value.
            if (x.getX().getValue() != -1) {
                xFormula = imgr.makeNumber(x.getX().getValue());
            }
            if (x.getY().getValue() != -1) {
                yFormula = imgr.makeNumber(x.getY().getValue());
            }
            if (x.getSolution().getValue() != -1) {
                zFormula = imgr.makeNumber(x.getSolution().getValue());
            }

            mathCrossword.getFormulaVariables().put(x.getX().getIndexInString(), xFormula);
            mathCrossword.getFormulaVariables().put(x.getY().getIndexInString(), yFormula);
            mathCrossword.getFormulaVariables().put(x.getSolution().getIndexInString(), zFormula);


            createInRangeConstraintsAndAddToConstraints(this, bmgr, imgr, x);

            BooleanFormula constraint;
            switch (x.getOperator()) {
                case '+':
                    constraint = imgr.equal(
                            imgr.add(
                                    mathCrossword.getFormulaVariables().get(x.getX().getIndexInString()),
                                    mathCrossword.getFormulaVariables().get(x.getY().getIndexInString())),
                            mathCrossword.getFormulaVariables().get(x.getSolution().getIndexInString()));
                    mathCrossword.getFormulaConstraints().add(constraint);

                    break;
                case '-':
                    constraint = imgr.equal(
                            imgr.subtract
                                    (mathCrossword.getFormulaVariables().get(x.getX().getIndexInString()),
                                            mathCrossword.getFormulaVariables().get(x.getY().getIndexInString())),
                            mathCrossword.getFormulaVariables().get(x.getSolution().getIndexInString()));
                    mathCrossword.formulaConstraints.add(constraint);
                    break;
                case '*':
                    constraint = imgr.equal(
                            imgr.multiply
                                    (mathCrossword.getFormulaVariables().get(x.getX().getIndexInString()),
                                            mathCrossword.getFormulaVariables().get(x.getY().getIndexInString())),
                            mathCrossword.getFormulaVariables().get(x.getSolution().getIndexInString()));
                    mathCrossword.getFormulaConstraints().add(constraint);
                    break;
                case '/':


                    BooleanFormula constraint3 = bmgr.and(
                            //divisor not zero
                            bmgr.not(imgr.equal(mathCrossword.getFormulaVariables().get(x.getSolution().getIndexInString()), imgr.makeNumber(0)))
                            ,
                            imgr.equal(
                                    imgr.divide(
                                            mathCrossword.getFormulaVariables().get(x.getX().getIndexInString()),
                                            mathCrossword.getFormulaVariables().get(x.getY().getIndexInString())),
                                    mathCrossword.getFormulaVariables().get(x.getSolution().getIndexInString())
                            ));

                    mathCrossword.getFormulaConstraints().add(constraint3);
                    break;
            }
        }
    }


    public static String readFile() {
        Path currentRelativePath = Paths.get("").toAbsolutePath();
        String fileName = currentRelativePath.getParent().toString() + "/src/input.txt";
        StringBuilder crosswordFromFile = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line;

            while ((line = reader.readLine()) != null) {
                crosswordFromFile.append(line).append("\n");
            }

            reader.close();

        } catch (IOException e) {
            System.out.println("Fehler beim Lesen der Datei: " + e.getMessage());
        }

        return crosswordFromFile.toString();
    }


    /**
     * creates the constraint, that the variable must be between 0 and 9 for every variable of a formula and
     * adds it to the formulaConstraints list in the MathCrossword class.
     *
     * @param mathCrossword class with the formulaConstraints list
     * @param bmgr          used to build formula-Constraints (equals, greaterOrEqual, lessOrEqual)
     * @param imgr          used to build formula-Constraints (logical - AND)
     * @param x             Formula-Object to extract the variables
     */
    public void createInRangeConstraintsAndAddToConstraints(MathCrossword mathCrossword, BooleanFormulaManager bmgr, IntegerFormulaManager imgr, Formula x) {
        BooleanFormula constraintX = bmgr.and(
                imgr.greaterOrEquals(mathCrossword.getFormulaVariables().get(x.getX().getIndexInString()), imgr.makeNumber(0)),
                imgr.lessOrEquals(mathCrossword.getFormulaVariables().get(x.getX().getIndexInString()), imgr.makeNumber(9)));
        getFormulaConstraints().add(constraintX);
        BooleanFormula constraintY = bmgr.and(
                imgr.greaterOrEquals(mathCrossword.getFormulaVariables().get(x.getY().getIndexInString()), imgr.makeNumber(0)),
                imgr.lessOrEquals(mathCrossword.getFormulaVariables().get(x.getY().getIndexInString()), imgr.makeNumber(9)));
        getFormulaConstraints().add(constraintY);
        BooleanFormula constraintZ = bmgr.and(
                imgr.greaterOrEquals(mathCrossword.getFormulaVariables().get(x.getSolution().getIndexInString()), imgr.makeNumber(0)),
                imgr.lessOrEquals(mathCrossword.getFormulaVariables().get(x.getSolution().getIndexInString()), imgr.makeNumber(9)));
        getFormulaConstraints().add(constraintZ);
    }


    /**
     * This method checks wether the character is a digit or a "_" field. If true it will look for an operator on
     * the field right or below and if it's true then it will create a Formula-Object and add it to the equations list.
     *
     * @param input String of the crossword class
     * @param index index in the crossword string (char that needs to be checked)
     */
    public void checkAndCreateFormula(String input, int index) {
        if (!indexIsUsable(input, index)) return;
        if (Character.isDigit(input.charAt(index)) || checkForUnknownField(input.charAt(index))) {
            if (characterRightIsOperator(input, index) && characterBelowIsOperator(input, index)) {
                getEquations().add(createFormulaAndReturnIt(input, index, Directions.SIDEWARDS));
                getEquations().add(createFormulaAndReturnIt(input, index, Directions.DOWNWARDS));
            } else if (characterRightIsOperator(input, index)) {
                getEquations().add(createFormulaAndReturnIt(input, index, Directions.SIDEWARDS));
            } else if (characterBelowIsOperator(input, index)) {
                getEquations().add(createFormulaAndReturnIt(input, index, Directions.DOWNWARDS));
            }
        }


    }

    /**
     * checks wether a character next to this index is an Operator (+,-,*,/). "Next to" is in this case
     * index + 1
     *
     * @param input String of the entire Crossword
     * @param index index of the character where the below character is going to be checked
     * @return true if the checked character is an Operator (+,-,*,/)
     */
    public boolean characterRightIsOperator(String input, int index) {
            return indexIsUsable(input, index) && characterIsOperator(input.charAt(index + 1));
    }

    /**
     * Unsafe method which returns the character which is below this character (index + length of a line in the crossword)
     *
     * @param input Crossword in String-format
     * @param index index of the Crossword-String
     * @return character below the character at the index
     */
    public char characterNextRow(String input, int index) {
        return input.charAt(index + lineLengthForNextRow);
    }

    /**
     * checks wether a character below this index is an Operator (+,-,*,/). "Below" is in this case
     * index + length of a line, because we assume that all lines have the same length.
     *
     * @param input String of the entire Crossword
     * @param index index of the character where the below character is going to be checked
     * @return true if the checked character is an Operator (+,-,*,/)
     */
    public boolean characterBelowIsOperator(String input, int index) {
        return indexIsUsable(input, index) && characterIsOperator(characterNextRow(input, index));
    }

    public boolean indexIsUsable(String input, int index) {
        return index <= input.length() && index >= 0
                && index + lineLengthForNextRow <= input.length();
    }

    /**
     * checks if a character is either +, -, * or /
     *
     * @param c checking character
     * @return true if it is contained
     */
    public boolean characterIsOperator(char c) {
        if (c == '+') {
            return true;
        }
        if (c == '-') {
            return true;
        }
        if (c == '*') {
            return true;
        }
        return c == '/';
    }

    /**
     * checks if a character is an "_" field
     *
     * @param c character that is being checked
     * @return true if it is an "_" field
     */
    public boolean checkForUnknownField(char c) {
        return c == '_';
    }

    /**
     * This method creates a Formula-Object and returns it. It reads the symbols (either next to or under the first
     * character) and wraps them into a single Formula-Object.
     *
     * @param input     The crossword String
     * @param index     the index of the first character of the formula (eg. 4 if the formula is 4 + 5 = 9)
     * @param direction the direction in which the formula should be read (either sidewards or downwards)
     * @return a Formula Object representing the formula
     */
    public Formula createFormulaAndReturnIt(String input, int index, Directions direction) {
        int x;
        int y;
        int z;
        char operator;
        if (checkForUnknownField(input.charAt(index))) x = -1;
        else x = Character.getNumericValue(input.charAt(index));


        if (direction == Directions.DOWNWARDS) {
            int checkedValueForY = index + 2 * lineLengthForNextRow;
            int checkedValueForZ  = index + 4 * lineLengthForNextRow;
            operator = characterNextRow(input, index);
            if (checkForUnknownField(input.charAt(checkedValueForY))) y = -1;
            else y = Character.getNumericValue(input.charAt(checkedValueForY));
            if (checkForUnknownField(input.charAt(checkedValueForZ))) z = -1;
            else z = Character.getNumericValue(input.charAt(checkedValueForZ));
        } else {
            operator = input.charAt(index + 1);
            if (checkForUnknownField(input.charAt(index + 2))) y = -1;
            else y = Character.getNumericValue(input.charAt(index + 2));
            if (checkForUnknownField(input.charAt(index + 4))) z = -1;
            else z = Character.getNumericValue(input.charAt(index + 4));
        }
        return new Formula(this, index, x, y, z, operator, direction);
    }


    /**
     * used to find out, how long a line is. We assume all lines have the same length (as portrayed in the example).
     * Counts the amount of chars until \n (spaces included)
     *
     * @param input String
     * @return amount of characters until first \n
     */
    public static int findOutLineLength(String input) {
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == '\n') {
                return i;
            }
        }
        return input.length();
    }

    public String getCrossword() {
        return crossword;
    }

    public int getLineLengthForNextRow() {
        return lineLengthForNextRow;
    }

    public ArrayList<Formula> getEquations() {
        return equations;
    }

    public HashMap<Integer, Variable> getVariables() {
        return variables;
    }

    public ArrayList<BooleanFormula> getFormulaConstraints() {
        return formulaConstraints;
    }

    public HashMap<Integer, NumeralFormula.IntegerFormula> getFormulaVariables() {
        return formulaVariables;
    }

}




