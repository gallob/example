package org.sosy_lab.java_smt_example;


public class Variable {
    private int value; //-1 if unknown
    private final int indexInString;
    public Variable (int pValue, int pIndexInString) {
        indexInString = pIndexInString;
        value = pValue;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getIndexInString() {
        return indexInString;
    }

}
